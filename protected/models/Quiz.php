<?php

/**
 * Модель для таблицы "pr_site_events".
 *
 * The followings are the available columns in table 'pr_site_events':
 * @property integer $id_site_events
 * @property string $title
 * @property string $url
 * @property string $start_date
 */
class Quiz extends DaActiveRecord {

  const ID_OBJECT = 'project-vopros-otvet';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return SiteEvents the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'pr_question_answer';
  }

    public function defaultScope() {
        $alias =  $this->getTableAlias(true, false);
        return array(
            'order' => 'sequence ASC'
        );
    }
}