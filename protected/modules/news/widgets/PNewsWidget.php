<?php
	Yii::import ('ygin.modules.news.widgets.news.NewsWidget');

	class PNewsWidget extends NewsWidget{
		public function getNews ($idCategory = PNewsController::ID_CATEGORY_AFISHA){
			return News::model ()->last ($this->maxNews)->findAll ('id_news_category = :ID_NEWS_CATEGORY',array(
				':ID_NEWS_CATEGORY' => $idCategory,
			));
		}

		public function run (){
			$this->render ('webroot.themes.business.views.NewsWidget.newsWidget');
		}
	}