<?php

	class PFeedback extends Feedback{
		public function rules (){
			return array(
				array(
					'fio, phone',
					'required'
				),
				array(
					'mail',
					'email',
					'message' => 'Введен некорректный e-mail адрес'
				),
				array(
					'verifyCode',
					'DaCaptchaValidator',
					'caseSensitive' => true
				),
				array(
					'fio, phone, mail',
					'length',
					'max' => 255
				),
			);
		}
	}