<?php if(!$books): ?>
	<div>Нет книг.</div>
	<?php return; ?>
<?php endif; ?>
<div>
	<ul>
		<?php foreach( $books as $book ): ?>
			<li>
				<div class="containt">
					<a class="pic" href="<?php echo Yii::app ()->createUrl ('book/default/view',array('id' => $book->primaryKey)); ?>">
						<?php if(false&&$preview = $book->getImagePreview ('_view')): ?>
							<img src="<?php echo $preview->getUrlPath (); ?>" alt="<?php echo $book->name; ?>">
						<?php endif; ?>
						<b><?php echo $book->author; ?></b>
						<?php echo $book->name; ?>
					</a>
				</div>
			</li>
		<?php endforeach; ?>
	</ul>
</div>
