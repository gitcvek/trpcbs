<table class="cReadTable">
	<tbody>
	<tr>
		<?php if($preview = $book->getImagePreview ('_view')): ?>
			<td class="item">
				<img src="<?php echo $preview->getUrlPath (); ?>" alt="<?php echo $book->name; ?>">
				<br>
			</td>
		<?php endif; ?>
		<td class="item">
			<div class="b-book-title"><?php echo $book->name; ?></div>
			<div class="b-book-author"><?php echo $book->author; ?></div>
			<div class="b-book-biblio">
				<p><?php echo $book->biblio; ?></p>
			</div>
			<div class="b-book-description"><p><?php echo $book->description; ?></p></div>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="border-bottom:none">
		</td>
	</tr>
	</tbody>
</table>

<div class="archive">←
	<a href="<?php echo Yii::app ()->createUrl ('book/default/index'); ?>">Архив книг</a>
</div>