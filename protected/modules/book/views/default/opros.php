<?php
	$this->setPageTitle('Опрос по независимой оценке качества услуг');
	$this->caption = 'Опрос по независимой оценке качества услуг';
?>
<p>Опрос по оценке качества оказания услуг библиотеками МБУК «Троицко-Печорская МЦБ»</p>
<p>Мы хотим узнать, как Вы оцениваете качество работы МБУК «Троико- Печорская МЦБ». Просим Вас с пониманием отнестись к анкетированию и внимательно ответить на задаваемые вопросы. Выберите один из вариантов на каждый вопрос. При этом не нужно указывать свое имя, Ваши личные данные нигде не прозвучат. Ваше мнение нам очень важно и будет учтено в дальнейшей работе.</p>
<?php
	if (Yii::app()->user->hasFlash('opros-success')) {
		$this->widget('AlertWidget', array(
			'title' => 'Опрос',
			'message' => Yii::app()->user->getFlash('opros-success'),
		));
	}

    $form = $this->beginWidget('CActiveForm', array(
        //'id' => 'feedbackForm-main-page',
        //'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'focus' => array($model, 'fio'),
        'htmlOptions' => array(
            'class' => 'form-horizontal',
            'enctype' => 'multipart/form-data',
            'style' => 'margin-top: 10px;',
        ),
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
        ),
        'errorMessageCssClass' => 'label label-important',
        'action' => Yii::app()->createUrl('book/default/opros'),
    ));
?>
<fieldset style="width: 90%; margin-left: 17px;">
	<?php echo Yii::app()->user->getFlash("opros-message"); ?>
	
	<div class="form-group">
		<label class="required">1. Как часто вы посещаете библиотеки</label>
        <?php echo $form->dropDownList($model, 'q1', $q1, array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'q1'); ?>
	</div>
	<div class="form-group">
		<label class="required">2. Удовлетворяют ли Вас качество, полнота и доступность информации о работе библиотек? Как Вы оцениваете свою информированность о работе библиотек, перечне предоставляемых услуг</label>
	        <?php echo $form->dropDownList($model, 'q2', $q2, array('class' => 'form-control')); ?>
	        <?php echo $form->error($model, 'q2'); ?>
	</div>
	<div class="form-group">
        <label class="required">Комментарий</label>

            <?php echo $form->textArea($model, 'comment', array('class' => 'form-control', 'rows' => '8', 'autocomplete' => 'off')); ?>
            <?php echo $form->error($model, 'comment'); ?>

    </div>
	<div class="form-group">
		<label class="required">3. Считаете ли Вы условия предоставления культурных услуг в библиотеках доступными, в том числе для инвалидов и других маломобильных групп населения</label>

	        <?php echo $form->dropDownList($model, 'q3', $q3, array('class' => 'form-control')); ?>
	        <?php echo $form->error($model, 'q3'); ?>
	</div>
	<div class="form-group">
        <label class="required">Комментарий</label>

            <?php echo $form->textArea($model, 'comment2', array('class' => 'form-control', 'rows' => '8', 'autocomplete' => 'off')); ?>
            <?php echo $form->error($model, 'comment2'); ?>
    </div>
	<div class="form-group">
		<label class="required">4. Удовлетворяют ли Вас благоустройство и содержание помещения библиотек и территории, на которой она расположена (внешний вид помещения библиотек, качество уборки помещений и территории библиотек, оформление и освещение, температурный режим и пр.)</label>
	        <?php echo $form->dropDownList($model, 'q4', $q4, array('class' => 'form-control')); ?>
	        <?php echo $form->error($model, 'q4'); ?>
	</div>
	<div class="form-group">
        <label class="required">Комментарий</label>
        
            <?php echo $form->textArea($model, 'comment3', array('class' => 'form-control', 'rows' => '8', 'autocomplete' => 'off')); ?>
            <?php echo $form->error($model, 'comment3'); ?>
    </div>
    <div class="form-group">
		<label class="required">5. Вы удовлетворены компетентностью работников библиотек (профессиональной грамотностью) при предоставлении Вам услуг? Довольны ли Вы работой сотрудников библиотеки</label>
		
	        <?php echo $form->dropDownList($model, 'q5', $q5, array('class' => 'form-control')); ?>
	        <?php echo $form->error($model, 'q5'); ?>
	</div>
    <div class="form-group">
		<label class="required">6. Считаете ли Вы, что работники библиотек вежливы, доброжелательны и внимательны</label>
		
	        <?php echo $form->dropDownList($model, 'q6', $q6, array('class' => 'form-control')); ?>
	        <?php echo $form->error($model, 'q6'); ?>
	</div>
	<div class="form-group">
		<label class="required">7. С какой целью Вы посещаете библиотеку</label>
		
	        <?php echo $form->dropDownList($model, 'q7', $q7, array('class' => 'form-control')); ?>
	        <?php echo $form->error($model, 'q7'); ?>
	</div>
	<div class="form-group">
        <label class="required">Комментарий</label>
        
            <?php echo $form->textArea($model, 'comment4', array('class' => 'form-control', 'rows' => '8', 'autocomplete' => 'off')); ?>
            <?php echo $form->error($model, 'comment4'); ?>
    </div>
	<div class="form-group">
		<label class="required">8. Удовлетворяет ли Вас качество предлагаемых библиотечных услуг</label>
		
	        <?php echo $form->dropDownList($model, 'q8', $q8, array('class' => 'form-control')); ?>
	        <?php echo $form->error($model, 'q8'); ?>
	</div>
	<div class="form-group">
        <label class="required">Комментарий</label>
        
            <?php echo $form->textArea($model, 'comment5', array('class' => 'form-control', 'rows' => '8', 'autocomplete' => 'off')); ?>
            <?php echo $form->error($model, 'comment5'); ?>
    </div>
	<div class="form-group">
		<label class="required">9. Посоветуете ли Вы своим родственникам и знакомым посетить библиотеку</label>
		
	        <?php echo $form->dropDownList($model, 'q9', $q9, array('class' => 'form-control')); ?>
	        <?php echo $form->error($model, 'q9'); ?>
	</div>
	<div class="form-group">
		<label class="required">10. Обеспечена ли возможность для инвалидов посадки в транспортное средство и высадки из него перед входом в организацию культуры, в том числе с использованием кресла-коляски</label>
		
	        <?php echo $form->dropDownList($model, 'q10', $q10, array('class' => 'form-control')); ?>
	        <?php echo $form->error($model, 'q10'); ?>
	</div>
	<div class="form-group">
		<label class="required">11. Оснащена ли организация специальными устройствами для доступа инвалидов (оборудование входных зон, раздвижные двери, приспособленные перила, доступных санитарно-гигиенических помещений, звуковые устройства для инвалидов по зрению и т.п.)</label>
		
	        <?php echo $form->dropDownList($model, 'q11', $q11, array('class' => 'form-control')); ?>
	        <?php echo $form->error($model, 'q11'); ?>
	</div>
	<div class="form-group">
		<label class="required">12. Размещена ли информация, необходимая для обеспечения беспрепятственного доступа инвалидов к учреждению и услугам (дублирование необходимой для получения услуги звуковой и зрительной информации, а также надписей, знаков и иной текстовой и графической информации знаками, выполненными рельефно-точечным шрифтом Брайля и на контрастном фоне)</label>
		
	        <?php echo $form->dropDownList($model, 'q12', $q12, array('class' => 'form-control')); ?>
	        <?php echo $form->error($model, 'q12'); ?>
	</div>
	<div class="form-group">
        <label class="required">13. Ваши предложения, пожелания по улучшению качества оказания услуг библиотеками</label>
        
            <?php echo $form->textArea($model, 'comment6', array('class' => 'form-control', 'rows' => '8', 'autocomplete' => 'off')); ?>
            <?php echo $form->error($model, 'comment6'); ?>
    </div>
    <div class="form-group">
    	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <div class="g-recaptcha" data-sitekey="6LflU5YaAAAAALwb1H6d7Vsi6ZDH-E8kvTHoXaUU"></div>
    </div>
    <div class="form-group">
       <?php echo CHtml::submitButton('Отправить', array('class' => 'btn btn-large btn-primary')); ?>
    </div>
</fieldset>
<?php $this->endWidget(); ?>