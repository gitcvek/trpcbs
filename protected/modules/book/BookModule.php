<?php

class BookModule extends DaWebModuleAbstract {

	protected $_urlRules = array(
	  'book/<id:\d+>' => 'book/default/view',
	  'book' => 'book/default/index',
	  'opros' => 'book/default/opros'
	);
	
	public function init() {
		$this->setImport(array(
			$this->id.'.models.*',
			$this->id.'.components.*',
		));
	}

}
