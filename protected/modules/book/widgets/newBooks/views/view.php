<div class="carousel booksCarousel">
	<div class="b-carousel-title">Хорошим книгам - добрый путь! Новинки!</div>
	<div id="carousel_1" class="slider">
		<ul>
			<?php foreach( $books as $book ): ?>
				<li>
					<div class="containt">
						<a class="pic" href="<?php echo Yii::app ()->createUrl ('book/default/view',array('id' => $book->primaryKey)); ?>">
							<?php if($preview = $book->getImagePreview ('_widget')): ?>
								<img src="<?php echo $preview->getUrlPath (); ?>" alt="<?php echo $book->name; ?>">
							<?php endif; ?>
							<b><?php echo $book->author; ?></b>
							<?php echo $book->name; ?>
						</a>
					</div>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
	<p class="controls">
		<input type="button" id="prev-slide" value="">
		<input type="button" id="next-slide" value="">
	</p>
</div>