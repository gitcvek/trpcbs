<?php

	class NewBooks extends DaWidget{
		public function run (){
			$books = Book::model ()->findAll ();
			if(!$books) return;
			$this->render ('view',array(
				'books' => $books,
			));
		}
	}