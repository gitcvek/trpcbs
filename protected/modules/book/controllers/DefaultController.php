<?php
	class DefaultController extends Controller{
		public $urlAlias = 'book';

		public function actionIndex (){
			$books = Book::model ()->findAll ();
			$this->render ('index',array(
				'books' => $books,
			));
		}

		public function actionView ($id){
			$book = $this->loadModelOr404 ('Book',$id);
			$this->render ('view',array(
				'book' => $book,
			));
		}

	    public function actionOpros() {
	        //Сохранение обращения
	        //После заполнения формы данные сохраняются в административном разделе сайта, а также приходит сообщение на почту с информацией о том, что заполнена форма по опросу.
	        if (isset($_POST['Opros'])) {
	        	//echo '1';
	            $success = $this->verifyCaptcha();
	            if ($success) {
	            	//echo '2';
	                $model = new Opros();
	                $model->attributes = $_POST['Opros'];
	                $model->onAfterSave = array($this, 'sendMessage');
	                if ($model->save()) {
	                	echo '3';
	                    /*
	                    [exception.CException] exception 'CException' with message 'Не определено свойство "DaWebApplication.notifier".' in /usr/www/syktyvdincbs.ru/yii/framework/base/CComponent.php:131
	                    $message = 'Здравствуйте!
	                    На сайт Сыктывдинской ЦБС поступили новые результаты опроса.';

	                    //Event::newInstance(302, null, $message, null, null);
	                    //$daPage->addPreloadJs('daAlert("Информация", "Большое спасибо за участие в опросе!", "ok", "msgSuccess");');
	                    //Если необходимо немедленно отправить письмо (например, при восстановлении пароля)
	                    Yii::app()->notifier->addNewEvent(302, $message)->sendNowLastAdded();*/

	                    Yii::app()->user->setFlash('opros-success', 'Большое спасибо за участие в опросе!');
	                    //$this->redirect(Yii::app()->user->returnUrl);
	                } else {
	                    // вообще сюда попадать в штатных ситуациях не должны
	                    // только если кул хацкер резвится
	                    Yii::app()->user->setFlash('opros-message', CHtml::errorSummary($model, '<p>Не удалось отправить форму</p>'));
	                    //echo CHtml::errorSummary($model, '<p>Не удалось отправить форму</p>');
	                }
	            }
	            //exit;
	        }

	        $this->render('opros', array(
	            'model' => new Opros(), 
	            'q1' => $this->getReferenceElements('project-reference-kak-chasto-vy-poseschaete-biblioteki'),
	            'q2' => $this->getReferenceElements('project-reference-udovletvoryayut-li-vas-dostupnost-informazii-o-rabote-bibliotek'),
	            'q3' => $this->getReferenceElements('project-reference-schitaete-li-vy-usloviya-predostavleniya-kulturnyh-uslug-v-bibliotekah-dostupnymi'),
	            'q4' => $this->getReferenceElements('project-reference-udovletvoryayut-li-vas-blagoustroistvo-i-soderghanie-pomescheniya-bibliotek'),
	            'q5' => $this->getReferenceElements('project-reference-vy-udovletvoreny-kompetentnostyu-rabotnikov-bibliotek'),
	            'q6' => $this->getReferenceElements('project-reference-schitaete-li-vy-chto-rabotniki-bibliotek-veghlivy-dobroghelatelny-i-vnimatelny'),
	            'q7' => $this->getReferenceElements('project-reference-absolyutno-net'),
	            'q8' => $this->getReferenceElements('project-reference-udovletvoryaet-li-vas-kachestvo-predlagaemyh-bibliotechnyh-uslug'),
	            'q9' => $this->getReferenceElements('project-reference-posovetuete-li-vy-svoim-rodstvennikam-i-znakomym-posetit-biblioteki'),
	            'q10' => $this->getReferenceElements('project-reference-obespechena-li-vozmoghnost-dlya-invalidov-posadki-v-transportnoe-sredstvo'),
	            'q11' => $this->getReferenceElements('project-reference-osnaschena-li-organizaziya-spezialnymi-ustroistvami-dlya-dostupa-invalidov'),
	            'q12' => $this->getReferenceElements('project-reference-razmeschena-li-informaziya-neobhodimaya-dlya-obespecheniya-dostupa-invalidov')
	        ));
	    }
  
	  	public function sendMessage(CEvent $event) {
		    Yii::app()->notifier->addNewEvent(105, $this->renderPartial('message_email', array('opros' => $event->sender), true));
	  	}

	    private function getReferenceElements($id) {
	        $es = [];

	        $elements = ReferenceElement::model()->findAll(['condition' => 'id_reference = "'.$id.'"']);
	        foreach ($elements as $e) {
	            $es[$e->id_reference_element] = $e->value;
	        }

	        return $es;
	    }

	    public function verifyCaptcha() {
	    	//31.03.2021: ключ зарегистрирован на fcbarcelona@cktv.ru (Елена Фёдорова)
	        if ( isset($_POST['g-recaptcha-response']) ){
	            $post_data = http_build_query(
	                array(
	                    'secret' => '6LflU5YaAAAAAB4YfTaX02xepXLwISFrFvgvfWeA',
	                    'response' => $_POST['g-recaptcha-response'],
	                    'remoteip' => $_SERVER['REMOTE_ADDR']
	                )
	            );
	            $opts = array('http' =>
	                array(
	                    'method' => 'POST',
	                    'header' => 'Content-type: application/x-www-form-urlencoded',
	                    'content' => $post_data
	                )
	            );
	            $context = stream_context_create($opts);
	            $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
	            $result = json_decode($response);
	        } else {
	            return false;
	        }

	        return $result->success;
	    }
	}