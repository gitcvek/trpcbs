<?php

/**
 * Модель для таблицы "pr_book".
 * The followings are the available columns in table 'pr_book':
 * @property integer $id_book
 * @property integer $cover
 * @property string  $author
 * @property string  $biblio
 * @property string  $description
 * @property string  $name
 * @property integer $visible
 * @property integer $sequence
 * The followings are the available model relations:
 * @property File    $coverFile
 */
class Book extends DaActiveRecord implements ISearchable{
    const ID_OBJECT = 'project-knigi';
    protected $idObject = self::ID_OBJECT;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return Book the static model class
     */
    public static function model ($className = __CLASS__){
        return parent::model ($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName (){
        return 'pr_book';
    }

    public function defaultScope() {
        $alias =  $this->getTableAlias(true, false);
        return array(
            'condition' => "$alias.visible = 1",
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules (){
        return array(
                array(
                    'cover, author, biblio, description, name',
                    'required'
                    ),
                array(
                    'cover, visible, sequence',
                    'numerical',
                    'integerOnly' => true
                    ),
                array(
                    'author, name',
                    'length',
                    'max' => 255
                    ),
                );
    }

    /**
     * @return array relational rules.
     */
    public function relations (){
        return array(
                'coverFile' => array(
                    self::BELONGS_TO,
                    'File',
                    'cover'
                    ),
                );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels (){
        return array(
                'id_book' => 'ID',
                'cover' => 'Обложка',
                'author' => 'Автор',
                'biblio' => 'Данные',
                'description' => 'Описание',
                'name' => 'Название',
                'visible' => 'Видимость',
                'sequence' => 'п/п',
                );
    }

    public function behaviors (){
        $behaviors = array(
                'ImagePreviewBehavior' => array(
                    'class' => 'ImagePreviewBehavior',
                    'imageProperty' => 'coverFile',
                    'formats' => array(
                        '_widget' => array(
                            'width' => 80,
                            'height' => 125,
                            ),
                        '_index' => array(
                            'width' => 200,
                            'height' => 200,
                            ),
                        '_view' => array(
                            'width' => 300,
                            'height' => 300,
                            ),
                        '_nocut' => array(
                            'width' => 400,
                            'height' => 400,
                            ),
                        ),
                    ),
                );

        return $behaviors;
    }

    public function getUrl (){
        if($this->id_book) {
            return Yii::app ()->createUrl ('book/default/view',array('id' => $this->id_book));
        }

        return Yii::app ()->createUrl ('book/default/index');
    }

    public function getSearchTitle (){
        return $this->name;
    }

    public function getSearchUrl (){
        return $this->getUrl ();
    }

    public function getCoverUrl() {
        if ($this->coverFile) {
            return $this->coverFile->getUrlPath();
        }
        return '';
    }
}
