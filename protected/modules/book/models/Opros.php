<?php
class Opros extends DaActiveRecord {	
	const ID_OBJECT = 'project-opros';
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @return Memorial the static model class
	 */
	public static function model ($className = __CLASS__) {
		return parent::model ($className);
	}

	public function rules()	{
		return array(
			array('q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12', 'required'),
            array('comment, comment2, comment3, comment4, comment5, comment6','safe')
		);
	}

	public function beforeSave() {
		if ($this->isNewRecord) $this->date = time();
		return true;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName () {
		return 'pr_opros';
	}
}