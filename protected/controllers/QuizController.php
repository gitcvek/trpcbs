<?php
Yii::import('application.models.Quiz');

class QuizController extends Controller {
    public function actionIndex() {
        $items = Quiz::model()->findAll();

        $this->render('index', array(
            'items' => $items
        ));
    }
}