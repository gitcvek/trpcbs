function initBlind(config) {
    $(document).ready(function () {
        addPanel();
        var needCookieJs = config.needCookieJs;
        var toBlindBtn = config.toBlindBtn;
        var toNormalVer = $('.to-normal-version');
        var fontUp = $('.b-blind-panel .up');
        var fontDown = $('.b-blind-panel .down');

        if (!needCookieJs) {
            initCookie();
        }

        $(toBlindBtn).click(function () {
            $.cookie('blind', 'true', {expires: 7, path: '/'});
            setCurrentVersion(toBlindBtn);
        });
        $(toNormalVer).click(function () {
            $.cookie("blind", null, {path: '/'});
            setCurrentVersion(toBlindBtn);
        });
        $(fontUp).click(function () {
            blindFontUp();
        });
        $(fontDown).click(function () {
            blindFontDown();
        });

        $('.js-blind-mode-black').click(function () {
            $.cookie('blind-color', 'blind-black', {expires: 7, path: '/'});
            setCurrentVersion(toBlindBtn);
        });
        $('.js-blind-mode-white').click(function () {
            $.cookie('blind-color', 'blind-white', {expires: 7, path: '/'});
            setCurrentVersion(toBlindBtn);
        });
        $('.js-blind-mode-red').click(function () {
            $.cookie('blind-color', 'blind-red', {expires: 7, path: '/'});
            setCurrentVersion(toBlindBtn);
        });

        //
        $('.cb-value').click(function () {
            var mainParent = $(this).parent('.img-toggle-btn');
            if ($(mainParent).find('input.cb-value').is(':checked')) { //включить
                $(mainParent).addClass('active');
                $.cookie("blind-images", 'true', {expires: 7, path: '/'});
                setCurrentVersion(toBlindBtn);
            } else {
                $(mainParent).removeClass('active');
                $.cookie("blind-images", null, {path: '/'});
                setCurrentVersion(toBlindBtn);
            }

        });
        //
        var imgButton = $('.img-toggle-btn');
        var imgCheckbox = $('.img-toggle-btn .cb-value');
        if ($.cookie('blind-images') == 'true') { //разрешить
            $(imgButton).addClass('active');
            $(imgCheckbox).attr('checked', 'checked');
        } else {
            $(imgButton).removeClass('active');
            $(imgCheckbox).removeAttr('checked');
        }
        //
        setCurrentVersion(toBlindBtn);

        $('.blind-version button.slick-prev, .blind-version button.slick-next').text('');
        var attr = $('.b-faq .ask-form label[for^="Question_verifyCode"]');
        attr.text('Код проверки *(чтобы увидеть, необходимо включить изображения)');
    });
}

function addPanel() {
    $("head").append($("<link rel='stylesheet' href='/blind/reset.css' type='text/css' />"));
    $("head").append($("<link rel='stylesheet' href='/blind/blind.css' type='text/css' />"));
    var blindPanel = '<div class="b-blind-panel"><div class="to-normal-version">Обычная версия сайта</div>' +
        '<div class="font-size">Размер шрифта: <span class="down">a-</span><span class="up">A+</span></div>' +
        '<div class="image">Изображения: <div class="img-toggle-btn"><input type="checkbox" class="cb-value"> <span class="round-btn"></span></div></div>' +
        '<div class="color-mode js-blind-mode-black">Ц</div>' + '<div class="color-mode js-blind-mode-white">Ц</div>' + '<div class="color-mode js-blind-mode-red">Ц</div>' +
        '</div>';
    $('body').append($(blindPanel));
}

function setCurrentVersion(toBlindBtn) {
    $body = $('body');
    if ($.cookie('blind') == 'true') {

        if ($.cookie('blind-color')) {
            var blindColor = $.cookie('blind-color');
            $body.removeClass('blind-black blind-red blind-white');
            $body.addClass('blind-version ' + blindColor);
        } else {
            $body.removeClass('blind-black blind-red blind-white');
            $body.addClass('blind-version blind-white');
        }

        if ($.cookie('blind-size')) {
            var blindSize = $.cookie('blind-size');
            $body.addClass(blindSize);
        }

        if ($.cookie('blind-images') == 'true') { //разрешить
            $('body').removeClass('img_none');
            console.log('blind-images true');
        } else {
            $('body').addClass('img_none');
            console.log('blind-images false');
        }

        $('.link__main-page').remove();
        $('.b-menu-top .nav').prepend('<li class="link__main-page"><a href="/">Главная</a></li>');

        $('.b-menu-top form').removeClass('hidden');
        $('.b-menu-top form .button-search').removeClass('hidden');
        $('.b-menu-top .search-icon').addClass('hidden');

        // $('.feedback-widget-captcha__help').remove();
        // $('.b-feedback-widget .form-group.feedback-widget-captcha').prepend('<span class="feedback-widget-captcha__help">Чтобы увидеть код проверки, включите изображения</span>');

        $('.b-blind-panel').slideDown();
        $(toBlindBtn).hide();
    } else {
        $('.feedback-widget-captcha__help').remove();
        $('.link__main-page').remove();
        $body.removeClass('blind-version img-none blind-black blind-red blind-white blind-font-down blind-font-down-1 blind-font-up blind-font-up-1');
        $('.b-blind-panel').slideUp();
        $(toBlindBtn).show();
    }
    //
    var $galleryPlaceholder = $(".gallery-placeholder");
    if ($galleryPlaceholder.size() > 0) {
        $(".b-event-single-gallery").appendTo("body").offset({top: $galleryPlaceholder.offset().top, left: 0});
    }
}

function blindFontUp() {
    if ($('body').hasClass('blind-font-up')) {
        $('body').addClass('blind-font-up-1');
        $('body').removeClass('blind-font-down blind-font-down-1 blind-font-up');
        $.cookie('blind-size', 'blind-font-up-1', {expires: 7, path: '/'});

    } else if ($('body').hasClass('blind-font-down')) {
        $('body').removeClass('blind-font-down blind-font-down-1 blind-font-up-1 blind-font-up');

    } else if ($('body').hasClass('blind-font-down-1')) {
        $('body').addClass('blind-font-down');
        $.cookie('blind-size', 'blind-font-down', {expires: 7, path: '/'});
        $('body').removeClass('blind-font-down-1 blind-font-up-1 blind-font-up');

    } else if ($('body').hasClass('blind-font-up-1')) {
        return false;
    } else {
        $('body').addClass('blind-font-up');
        $.cookie('blind-size', 'blind-font-up', {expires: 7, path: '/'});
    }
}

function blindFontDown() {
    if ($('body').hasClass('blind-font-up')) {
        $('body').removeClass('blind-font-down blind-font-down-1 blind-font-up-1 blind-font-up');

    } else if ($('body').hasClass('blind-font-down')) {
        $('body').addClass('blind-font-down-1');
        $.cookie('blind-size', 'blind-font-down-1', {expires: 7, path: '/'});
        $('body').removeClass('blind-font-down blind-font-up-1 blind-font-up');

    } else if ($('body').hasClass('blind-font-down-1')) {
        return false;

    } else if ($('body').hasClass('blind-font-up-1')) {
        $('body').addClass('blind-font-up');
        $.cookie('blind-size', 'blind-font-up', {expires: 7, path: '/'});
        $('body').removeClass('blind-font-down-1 blind-font-up-1 blind-font-down');
    } else {
        $('body').addClass('blind-font-down');
        $.cookie('blind-size', 'blind-font-down', {expires: 7, path: '/'});
    }
}

function initCookie() {
    (function (factory) {
        if (typeof define === 'function' && define.amd) {
            define(['jquery'], factory);
        } else if (typeof exports === 'object') {
            factory(require('jquery'));
        } else {
            factory(jQuery);
        }
    }(function ($) {

        var pluses = /\+/g;

        function encode(s) {
            return config.raw ? s : encodeURIComponent(s);
        }

        function decode(s) {
            return config.raw ? s : decodeURIComponent(s);
        }

        function stringifyCookieValue(value) {
            return encode(config.json ? JSON.stringify(value) : String(value));
        }

        function parseCookieValue(s) {
            if (s.indexOf('"') === 0) {
                s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
            }

            try {
                s = decodeURIComponent(s.replace(pluses, ' '));
                return config.json ? JSON.parse(s) : s;
            } catch (e) {
            }
        }

        function read(s, converter) {
            var value = config.raw ? s : parseCookieValue(s);
            return $.isFunction(converter) ? converter(value) : value;
        }

        var config = $.cookie = function (key, value, options) {

            if (value !== undefined && !$.isFunction(value)) {
                options = $.extend({}, config.defaults, options);

                if (typeof options.expires === 'number') {
                    var days = options.expires, t = options.expires = new Date();
                    t.setTime(+t + days * 864e+5);
                }

                return (document.cookie = [
                    encode(key), '=', stringifyCookieValue(value),
                    options.expires ? '; expires=' + options.expires.toUTCString() : '',
                    options.path ? '; path=' + options.path : '',
                    options.domain ? '; domain=' + options.domain : '',
                    options.secure ? '; secure' : ''
                ].join(''));
            }

            var result = key ? undefined : {};
            var cookies = document.cookie ? document.cookie.split('; ') : [];

            for (var i = 0, l = cookies.length; i < l; i++) {
                var parts = cookies[i].split('=');
                var name = decode(parts.shift());
                var cookie = parts.join('=');

                if (key && key === name) {
                    result = read(cookie, value);
                    break;
                }
                if (!key && (cookie = read(cookie)) !== undefined) {
                    result[name] = cookie;
                }
            }

            return result;
        };

        config.defaults = {};

        $.removeCookie = function (key, options) {
            if ($.cookie(key) === undefined) {
                return false;
            }
            $.cookie(key, '', $.extend({}, options, {expires: -1}));
            return !$.cookie(key);
        };

    }));

}

