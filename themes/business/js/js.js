function makeZoomOfic() {
$('.to_zoom').jqzoom({
  zoomType: 'innerzoom',
  zoomWidth: 300,
  zoomHeight: 250,
  title: false
});
}

function slider() {

  $('.b-menu-side-list li:has("ul") > a').toggle(function() {
    $(this).parent().children('ul').show();
	$(this).addClass('active');
  }, function() {
    $(this).parent().children('ul').hide();
	$(this).removeClass('active');
  });

}

$(document).ready(function() {
    initBlind({
        needCookieJs: false, // Подключен или нет jQueryCookie. Не обязательно (По умолчанию false - будет подключен автоматом)
        toBlindBtn: '.js-blind-selector' // Кнопка/ссылка включающая режим слабовидящих. Обязательный параметр
    });

    if(document.cookie.match('cookie-agreement') === null){
        $('.js-cookie-window').show();
    }

    $('.js-cookie-window-btn').click(function() {
        document.cookie = "cookie-agreement=1";
        $('.js-cookie-window').hide();
    });
});
