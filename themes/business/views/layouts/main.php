<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="content-language" content="ru"> <?php // TODO - в будущем генетить автоматом ?>
    <meta
	name="sputnik-verification"
	content="YtnV7CSIGBZrT0Zk"
/>
    <?php
    //Регистрируем файлы скриптов в <head>
    if (YII_DEBUG) {
        Yii::app()->assetManager->publish(YII_PATH . '/web/js/source', false, -1, true);
    }
    Yii::app()->clientScript->registerCoreScript('jquery');

    $this->registerJsFile('bootstrap.min.js', 'ygin.assets.bootstrap.js');
    $this->registerJsFile('modernizr-2.6.1-respond-1.1.0.min.js', 'ygin.assets.js');
    Yii::app()->clientScript->registerScriptFile('/themes/business/js/slick.min.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/themes/business/js/js.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/blind/blind.js', CClientScript::POS_HEAD);
    //    Yii::app()->clientScript->registerScriptFile('/themes/business/js/jq.carousel.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScript('menu.init', "$('.dropdown-toggle').dropdown();", CClientScript::POS_READY);
    Yii::app()->clientScript->registerScript('sub.menu.init', "slider();", CClientScript::POS_READY);

    $this->registerCssFile('bootstrap.min.css', 'application.assets.bootstrap.css');
    $ass = Yii::getPathOfAlias('application.assets.bootstrap.fonts') . DIRECTORY_SEPARATOR;
    Yii::app()->clientScript->addDependResource('bootstrap.min.css', array(
        $ass . 'glyphicons-halflings-regular.eot' => '../fonts/',
        $ass . 'glyphicons-halflings-regular.svg' => '../fonts/',
        $ass . 'glyphicons-halflings-regular.woff' => '../fonts/',
        $ass . 'glyphicons-halflings-regular.ttf' => '../fonts/',
    ));

    Yii::app()->clientScript->addDependResource('icons.css', array(
        $ass . 'icons.eot' => '../fonts/',
        $ass . 'icons.svg' => '../fonts/',
        $ass . 'icons.woff' => '../fonts/',
        $ass . 'icons.ttf' => '../fonts/',
    ));

    Yii::app()->clientScript->registerCssFile('/themes/business/css/icons.css');
    Yii::app()->clientScript->registerCssFile('/themes/business/css/content.css');
    Yii::app()->clientScript->registerCssFile('/themes/business/css/page.css');
    Yii::app()->clientScript->registerCssFile('/themes/business/css/slick.css');


    ?>
    <script async src="https://culturaltracking.ru/static/js/spxl.js?pixelId=8125" data-pixel-id="8125"></script>
    <title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
</head>
<body>
<div id="wrap" class="container">
    <div id="head" class="row">
        <?php if (Yii::app()->request->url == "/") { ?>
            <div class="logo col-lg-2 col-md-2 col-sm-3 col-xs-4"><img border="0" alt="Название компании - На главную"
                                            src="/themes/business/gfx/logo.png"></div>
        <?php } else { ?>
            <a href="/" title="Главная страница" class="logo col-lg-2 col-md-2 col-sm-3 col-xs-4"><img src="/themes/business/gfx/logo.png"
                                                                            alt="Логотип компании"></a>
        <?php } ?>
        <div class="cname col-lg-7 col-md-6 col-xs-8">Троицко-Печорская межпоселенческая<br>центральная библиотека
            <p>им. Г.А. Федорова</p>
        </div>
        <div class="tright col-lg-3 col-md-4">
            <div class="adress">
                <span><span class="icon-letter-mail"></span> Троицко-Печорск, ул.Мира-26</span>
            </div>
            <div class="numbers">
                <span><span class="glyphicon glyphicon-earphone"></span> +7-82138-97-3-63</span><span
                        class="b-vk-widget"><a target="_blank" href="https://vk.com/club29669960"><img class="vk-widget"
                                                                                                       src="/themes/business/gfx/vk_logo.png"> </a></span>
                <br>
                <span class="glyphout">+7-82138-97-4-65</span>
            </div>
        </div>
        <div title='Версия для слабовидящих' class="col-md-4 js-blind-selector">Версия для слабовидящих</div>
        <?php
        if (Yii::app()->hasModule('search')) {
            $this->widget('SearchWidget');
        }
        ?>
    </div>


    <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_TOP)); ?>

    <div class="b-menu-top navbar">
        <div class="nav-collapse">
            <?php

            $this->widget('MenuWidget', array(
                'rootItem' => Yii::app()->menu->all,
                'htmlOptions' => array('class' => 'nav nav-pills'), // корневой ul
                'submenuHtmlOptions' => array('class' => 'dropdown-menu'), // все ul кроме корневого
                'activeCssClass' => 'active', // активный li
                'activateParents' => 'true', // добавлять активность не только для конечного раздела, но и для всех родителей
                //'labelTemplate' => '{label}', // шаблон для подписи
                'labelDropDownTemplate' => '{label} <b class="caret"></b>', // шаблон для подписи разделов, которых есть потомки
                //'linkOptions' => array(), // атрибуты для ссылок
                'linkDropDownOptions' => array('data-target' => '#', 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'), // атрибуты для ссылок для разделов, у которых есть потомки
                //'itemOptions' => array(), // атрибуты для li
                'itemDropDownOptions' => array('class' => 'dropdown'),  // атрибуты для li разделов, у которых есть потомки
                'maxChildLevel' => 0,
                'encodeLabel' => false,
            ));

            ?>
        </div>
    </div>

    <?php // + Главный блок ?>
    <div id="main">

        <div id="container" class="row">
            <?php

            $column1 = 0;
            $column2 = 12;
            $column3 = 0;

            if (Yii::app()->menu->current != null) {
                $column1 = 3;
                $column2 = 6;
                $column3 = 3;

                if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_LEFT) == 0) {
                    $column1 = 0;
                    $column3 = 4;
                }
                if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_RIGHT) == 0) {
                    $column3 = 0;
                    $column1 = $column1 * 4 / 3;
                }
                $column2 = 12 - $column1 - $column3;
            }

            ?>
            <?php if ($column1 > 0): // левая колонка ?>
                <div id="sidebarLeft" class="col-md-<?php echo $column1; ?>">
                    <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_LEFT)); ?>
                </div>
            <?php endif ?>

            <div id="content" class="col-md-<?php echo $column2; ?>">
                <?php if (Yii::app()->getRequest()->getPathInfo() == '') $this->widget('BlockWidget', array("place" => SiteModule::PLACE_CONTENT)); ?>
                <div class="page-header">
                    <h1><?php echo $this->caption; ?></h1>
                </div>

                <?php if (isset($this->breadcrumbs)): // Цепочка навигации ?>
                    <?php unset($this->breadcrumbs['Левое меню']); // Аццкий зашивон, убираем из навигации левое меню ?>
                    <?php $this->widget('BreadcrumbsWidget', array(
                        'homeLink' => array('Главная' => Yii::app()->homeUrl),
                        'links' => $this->breadcrumbs,
                    )); ?>
                <?php endif ?>

                <div class="cContent">
                    <?php echo $content; ?>
                </div>
                <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_BOTTOM)); ?>
            </div>

            <?php if ($column3 > 0): // левая колонка ?>
                <div id="sidebarRight" class="col-md-<?php echo $column3; ?>">
                    <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_RIGHT)); ?>
                </div>
            <?php endif ?>

        </div>
        <?php //Тут возможно какие-нить модули снизу ?>
        <div class="clr"></div>
    </div>
    <?php // - Главный блок ?>

</div>


<div id="footer" class="container">
    <div class="row">
        <div class="col-md-5 logo">
            <img alt="Логотип компании" src="/themes/business/gfx/logo.png">
            <div class="cname">Троицко-Печорская межпоселенческая<br>центральная библиотека
                <p>им. Г.А. Федорова</p>
            </div>
        </div>
        <div class="col-md-5">
            <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_FOOTER)); ?>
        </div>
        <div class="col-md-2">
            <div id="cvek"><a title="создать сайт в Цифровом веке" href="http://cvek.ru">Создание сайта — веб-студия
                    &laquo;Цифровой век&raquo;</a></div>
            <div id="metrika">
                <!-- Yandex.Metrika informer -->
                <a href="https://metrika.yandex.ru/stat/?id=27724185&amp;from=informer"
                   target="_blank" rel="nofollow"><img
                            src="//bs.yandex.ru/informer/27724185/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
                            style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика"
                            title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"
                            onclick="try{Ya.Metrika.informer({i:this,id:27724185,lang:'ru'});return false}catch(e){}"/></a>
                <!-- /Yandex.Metrika informer -->
            </div>
            <div id="sputnik">
                <script type="text/javascript">
                   (function(d, t, p) {
                       var j = d.createElement(t); j.async = true; j.type = "text/javascript";
                       j.src = ("https:" == p ? "https:" : "http:") + "//stat.sputnik.ru/cnt.js";
                       var s = d.getElementsByTagName(t)[0]; s.parentNode.insertBefore(j, s);
                   })(document, "script", document.location.protocol);
                </script>
                <span id="sputnik-informer"></span>
            </div>
        </div>
    </div>
</div>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter27724185 = new Ya.Metrika({
                    id: 27724185,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="//mc.yandex.ru/watch/27724185" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->

<div class="b-cookie-window js-cookie-window" style="display: none;">
    <div class="container">
        <div style="margin-bottom: 15px;"><b>Мы используем cookie</b></div>
        <div>
            <div class="text">Во время посещения вами данного сайта МБУК «Троицко-Печорская МЦБ» вы соглашаетесь с тем, что мы
                обрабатываем
                ваши персональные данные с использованием метрических программ.
                <a href="/page/cookie/" target="_blank">Подробнее.</a>
            </div>
            <button class="cookie-btn js-cookie-window-btn">Понятно, спасибо</button>
        </div>
    </div>
</div>

</body>
</html>
