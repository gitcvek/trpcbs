<table class="cReadTable">
	<tbody>
	<tr>
		<?php if ($cover = $book->getCoverUrl()) : ?>
			<td class="item">
                <div class="b-book-cover">
				    <img src="<?php echo $cover; ?>" alt="<?php echo $book->name; ?>">
                </div>
				<br>
			</td>
		<?php endif; ?>
		<td class="item">
			<div class="b-book-author"><?php echo $book->author; ?>.</div>
			<div class="b-book-title"><?php echo $book->name; ?></div>
			<div class="b-book-biblio"><?php echo $book->biblio; ?></div>
			<div class="b-book-description"><?php echo $book->description; ?></div>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="border-bottom:none">
		</td>
	</tr>
	</tbody>
</table>
<div class="archive">←
	<a href="<?php echo Yii::app ()->createUrl ('book/default/index'); ?>">Архив книг</a>
</div>
