<?php if(!$books): ?>
	<div>Нет книг.</div>
	<?php return; ?>
<?php endif; ?>

<style>
.pic img {
    max-width: 100px;
}
</style>

<div class="b-book-archive-list">
	<ul>
		<?php foreach( $books as $book ): ?>
			<li>
				<div class="containt">
					<a class="pic" href="<?php echo Yii::app ()->createUrl ('book/default/view',array('id' => $book->primaryKey)); ?>">
						<?php if($cover = $book->getCoverUrl()): ?>
							<img src="<?php echo $cover; ?>" alt="<?php echo $book->name; ?>">
						<?php endif; ?>
						<b><?php echo $book->author; ?></b>
						<?php echo $book->name; ?>
					</a>
				</div>
			</li>
		<?php endforeach; ?>
	</ul>
</div>
