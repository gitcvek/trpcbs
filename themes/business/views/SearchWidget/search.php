<div class="b-searchform">
<form action="<?=Yii::app()->createUrl(SearchModule::ROUTE_SEARCH_VIEW)?>" class="navbar-form" role="search">
    <div class="form-group">
      <input type="search" id="query" name="query" class="b-search-input form-control query-input" placeholder="Поиск...">
    </div>
    <button class="b-search-btn btn btn-default button-search" type="submit"><span class="glyphicon glyphicon-search"></span></button>
</form>
</div>