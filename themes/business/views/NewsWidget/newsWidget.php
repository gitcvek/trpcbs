<?php
	$this->registerCssFile ('newsWidget.css');
?>
<div class="b-news-widget">
	<div class="b-button-widget">
		<a class="b-button-widget-btn" href="#"><span class="b-button-widget-icon icon-list"></span>Афиша</a>
	</div>
	<?php foreach( $this->getNews () as $model ): ?>
		<?php $this->render ('webroot.themes.business.views.news._list_item',array('model' => $model)); ?>
	<?php endforeach; ?>
	<div class="archive">
		<a href="<?php echo Yii::app ()->createUrl ('news/news/index',array('idCategory' => PNewsController::ID_CATEGORY_AFISHA)); ?>">Все афиши &nbsp;»</a>
	</div>
</div>