<div class="b-widget">
	<div class="b-widget-header">
		<a href="<?=$photoLink?>">Фотогалерея</a>
	</div>
	<?php
	$this->registerCssFile('photogallery-random-widget.css');
	?>
	<div class="b-widget-content b-photogallery-random-widget">
	  <?php if ($photo): ?>
	  <a href="<?=$photoLink?>"><img src="<?=$photo->getUrlPath()?>"></a>
	  <?php endif; ?>
<!--	  <div class="b-widget-control">-->
<!--		<a href="#"><span class="icon-left-circle"></span></a>-->
<!--	  	<a class="photogallery-link" href="#"><span class="icon-iphone-home"></span></a>	  	-->
<!--		<a href="#"><span class="icon-right"></span></a>-->
<!--	  </div>-->
	</div>
</div>