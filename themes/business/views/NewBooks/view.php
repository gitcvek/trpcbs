<?php
Yii::app()->clientScript->registerScript('bookWidget', '
  $(".booksCarousel").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
  });
 ', CClientScript::POS_READY);
?>

<div class="b-carousel-title">Библиороссыпь новинок</div>
<div class="booksCarousel">
	<?php foreach( $books as $book ): ?>
		<div class="b-carousel-item-container containt">
			<a class="b-carousel-itemlink" href="<?php echo Yii::app ()->createUrl ('book/default/view',array('id' => $book->primaryKey)); ?>">
				<?php if($preview = $book->getImagePreview ('_widget')): ?>
					<img src="<?php echo $preview->getUrlPath (); ?>" alt="<?php echo $book->name; ?>">
				<?php endif; ?>
				<br>
				<span class="title"><?php echo $book->name; ?></span>
				<?php echo $book->author; ?>
			</a>
		</div>
	<?php endforeach; ?>
  </div>
