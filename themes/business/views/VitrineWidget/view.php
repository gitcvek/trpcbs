<?php $this->registerCssFile('vitrine.css');?>
<?php if (Yii::app()->request->url != '/') return; // только на главной ?>

<div id="ygin-vitrine-carousel" class="b-vitrine carousel slide">

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
<?php
$i = 0;
foreach ($models AS $model) {
  $id    = $model->id_vitrine;
  $class = ($i == 0) ? 'item active' : 'item';
  $link  = $model->link;
  $imgHTML = '';
  if ($model->file) {
    $imgHTML = '<img src="'.$model->file->getUrlPath().'" alt="'.$model->title.'">';
  } else {
    $imgHTML = '<div class="img"></div>';
  }
?>
    <div class="<?= $class ?>">
      <?php echo $imgHTML ?>
    </div>
<?php
  $i++;
}
?>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#ygin-vitrine-carousel" data-slide="prev">
    <span class="icon-left-open"></span>
  </a>
  <a class="right carousel-control" href="#ygin-vitrine-carousel" data-slide="next">
    <span class="icon-right-open"></span>
  </a>
</div>