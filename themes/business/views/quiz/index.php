<?php
	$this->caption = 'Часто задаваемые вопросы';
	$this->pageTitle = 'Часто задаваемые вопросы';

	foreach ($items as $quiz) {
?>
	<div class="quiz">
<?php
		echo '<div class="question">'.
'<div class="text">'.$quiz->question.'</div>'.
'<div role="button" class="down" aria-label="Развернуть" tabindex="0" data-tooltip="Развернуть" data-tooltip-vertical-offset="-12" data-tooltip-horizontal-offset="0"><svg width="24" height="24" viewBox="0 0 24 24" focusable="false" fill="#c96500"><path d="M5.41 7.59L4 9l8 8 8-8-1.41-1.41L12 14.17"></path></svg></div>'.
'<div role="button" class="up" aria-label="Свернуть" tabindex="0" data-tooltip="Свернуть" data-tooltip-vertical-offset="-12" data-tooltip-horizontal-offset="0"><svg width="24" height="24" viewBox="0 0 24 24" focusable="false" fill="#c96500"><path d="M18.59 16.41L20 15l-8-8-8 8 1.41 1.41L12 9.83"></path></svg></div>'.
'</div>';
		echo '<div class="answer">'.$quiz->answer.'</div>';
?>
	</div>
<?php
	}
?>
<script>
	$(function() {
		$('.question').click(function() {
			$(this).next('.answer').toggle();
			$(this).find('.up, .down').toggle();
		});
	})
</script>