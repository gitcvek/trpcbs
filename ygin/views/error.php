<?php
$this->pageTitle=Yii::app()->name . ' - Error';
?>

<h2>Error <?php echo $code; ?></h2>

<div class="alert alert-error">
    <h3><?php echo CHtml::encode($type); ?></h3>
    <p><?php echo CHtml::encode($message); ?></p>
    <p><?php echo CHtml::encode($file); ?> line:<?php echo CHtml::encode($line); ?></p>
    <p><?php echo CHtml::encode($trace); ?></p>
</div>