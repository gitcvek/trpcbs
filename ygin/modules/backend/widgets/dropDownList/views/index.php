<?php

/**
 * @var $form CActiveForm
 * @var $this DropDownListWidget
 */

if ($this->getCountData() <= $this->maxData) {
    echo $form->dropDownList($model, $attributeName, $this->data, $this->htmlOption);
    echo $form->error($model, $attributeName);
    return;
}

?>
<div class="b-typeahead-widget">
    <div class="row">
        <div class="col-sm-6">

    <?php $this->widget('ygin.ext.typeahead.TbTypeAhead',array(
        'model' => $model,
        'attribute' => $attributeName,
        'enableHogan' => true,
        'options' => [
            [
                //'name' => 'countries',
                //'valueKey' => 'name',
                'remote' => array(
                    'url' => Yii::app()->createUrl('backend/engine/autocomplete').'?query=%QUERY&'.http_build_query([
                        'idObject' => $this->getIdObject(),
                        'limit'    => 100,
                    ]),
                ),
                'template' => '<p><b>{{label}}</b> [id={{value}}]</p>',
                'engine' => new CJavaScriptExpression('Hogan'),
                'limit'  => 100,
            ],
        ],
        'events' => array(
            'selected' => new CJavascriptExpression("function(obj, datum, name) {
                $(this).parents('.b-typeahead-widget').find('.ta-value').val(datum.label);
             }"),
        ),
    )); ?>

        </div>
        <div class="col-sm-6">
            <input class="field-edit form-control ta-value" disabled="disabled" value="<?= $this->getValueString() ?>">
        </div>
    </div>
</div>
